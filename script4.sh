#!/bin/bash

read -p 'entrer nom nv zip' nom_archive
var=$(ls -a /tmp/in | sed -e "/\.$/d" | wc -l)
echo "$nom_archive" >> /tmp/out/log

if [ -e /tmp/out lock ]
then
    echo "22"
    exit 22
else
    if [ $var -eq 0 ]
    then
        echo "dossier vide"
        exit 1
    else
        tar -czvf $nom_archive.tar.gz /tmp/in
        mv $nom_archive.tar.gz /tmp/out
	
    fi
fi
